// Server Program
#include "stepperMotorServer.h"

// Program Main
int main(int argc, char* argv[]) {

  // Connecting client information
	struct sockaddr_in clientAddress;
	socklen_t cliLen = sizeof(clientAddress);

	//Signal interrupt handler
	signal(SIGINT, interruptHandler);

	// init globals as defaults
	backlog = DEF_BACKLOG;
	numThreads = DEF_NUM_THREADS;
	port = DEF_PORT;
	bufferlen = DEF_BUFFER_LENGTH;
	ipLimit = SAME_IP_LIMIT;
	ioTimeout.tv_sec = DEF_IO_TIMEOUT_SEC;
	ioTimeout.tv_nsec = DEF_IO_TIMEOUT_NSEC;
	pingTimeout.tv_sec = DEF_PING_TIMEOUT_SEC;
	pingTimeout.tv_nsec = DEF_PING_TIMEOUT_NSEC;

	// motor
	motorCount = 0;
	stepLimit = DEF_STEP_LIMIT;

	// wiringPi
	if (initWiringPi() == ERROR) {
		error("ERROR on setting up wiringPi\n");
	}

	parseArgs(argc, argv);

	listenFD = makeServerSocket(port);
	pool = initThreadPool();
	sockets = initSocketList();
	connected = initSocketRefList();

	printf("\nAdding Threads...");
	for (int t = 0; t < numThreads; t++) {
		addThread(&pool);
	}
	printf("\n%i threads added to pool", numThreads);

	while (true) {

		// Accept new connection
		printf("\nReady and waiting for a new connection...\n");
		int newSockFD = accept(listenFD, (struct sockaddr *) &clientAddress, &cliLen);
		if (newSockFD == ERROR) {
			error("ERROR on accept\n");
		} else {
			char* socketIP = inet_ntoa(clientAddress.sin_addr);
			printf("\n%s:%d Connected\n", socketIP, (int)ntohs(clientAddress.sin_port));
			if (!connectedTooManyTimes(socketIP, &connected, ipLimit)) {
				addSocket(&sockets, newSockFD, socketIP, &connected);
			} else {
				close(newSockFD);
				printf("Connection rejected, connected too many times\n");
			}
		}
	}
}

/**
  * @breif - Parses the given CLI arguments (argv) and sets global vars acordingly.
  * @param - int argc - the number of CLI arguments.
  * @param - char* argv[] - the CLI arguments.
  */
void parseArgs(int argc, char* argv[]) {
	char* tempStrtolEndPtr;

	for (int i = 1; i < argc; i++) {

		if ((strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0)) { // Help argument
			help();
			exit(EXIT_SUCCESS);
		} else if ((strcmp(argv[i], "-p") == 0) || (strcmp(argv[i], "--port") == 0)) { // Port argument
			i++;  // move to input argument
			if (i >= argc
				|| (port = strtol(argv[i], &tempStrtolEndPtr, 10)) <= 0
				|| checkStrtolError(&port, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing port argument\n");
			}
		} else if ((strcmp(argv[i], "-b") == 0) || (strcmp(argv[i], "--backlog") == 0)) { // Backlog argument
			i++;  // move to input argument
			if (i >= argc
				|| (backlog = strtol(argv[i], &tempStrtolEndPtr, 10)) <= 0
				|| checkStrtolError(&backlog, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing backlog argument\n");
			}
		} else if ((strcmp(argv[i], "-t") == 0) || (strcmp(argv[i], "--threads") == 0)) { // Threads argument
			i++;  // move to input argumentt
			if (i >= argc
				|| (numThreads = strtol(argv[i], &tempStrtolEndPtr, 10)) <= 0
				|| checkStrtolError(&numThreads, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing threads argument\n");
			}
		} else if ((strcmp(argv[i], "-bs") == 0) || (strcmp(argv[i], "--buffer-size") == 0)) { // Buffer Length argument
			i++;  // move to input argumentt
			if (i >= argc
				|| (bufferlen = strtoll(argv[i], &tempStrtolEndPtr, 10)) <= 0
				|| checkStrtolError(&bufferlen, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing buffer length argument\n");
			}
		} else if ((strcmp(argv[i], "-il") == 0) || (strcmp(argv[i], "--ip-limit") == 0)) { // ip limit argument
			i++;  // move to input argumentt
			if (i >= argc
				|| (ipLimit = strtol(argv[i], &tempStrtolEndPtr, 10)) <= 0
				|| checkStrtolError(&ipLimit, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing ip limit argument\n");
			}
		} else if ((strcmp(argv[i], "-pt") == 0) || (strcmp(argv[i], "--ping-timeout") == 0)) { // ping timeout argument
			i++;  // move to input argumentt
			if (i >= argc
				|| (pingTimeout.tv_sec = strtol(argv[i], &tempStrtolEndPtr, 10)) < 0
				|| checkStrtolError(&pingTimeout.tv_sec, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing ping timeout seconds argument\n");
			}
			i++;  // move to second input argumentt
			if (i >= argc
				|| (pingTimeout.tv_nsec = strtol(argv[i], &tempStrtolEndPtr, 10)) < 0
				|| checkStrtolError(&pingTimeout.tv_nsec, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing ping timeout nanoseconds argument\n");
			}
		} else if ((strcmp(argv[i], "-it") == 0) || (strcmp(argv[i], "--io-timeout") == 0)) { // input output timeout argument
			i++;  // move to input argumentt
			if (i >= argc
				|| (ioTimeout.tv_sec = strtol(argv[i], &tempStrtolEndPtr, 10)) < 0
				|| checkStrtolError(&ioTimeout.tv_sec, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing io timeout seconds argument\n");
			}
			i++;  // move to second input argumentt
			if (i >= argc
				|| (ioTimeout.tv_nsec = strtol(argv[i], &tempStrtolEndPtr, 10)) < 0
				|| checkStrtolError(&ioTimeout.tv_nsec, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing io timeout nanoseconds argument\n");
			}
		}

		 // WiringPi CLI argument processing
		 else if ((strcmp(argv[i], "-pwt") == 0) || (strcmp(argv[i], "--pin-write-timeout") == 0)) { // pin lock timeout argument
			i++;  // move to input argumentt
			if (i >= argc
				|| (pinWriteTimeout.tv_sec = strtol(argv[i], &tempStrtolEndPtr, 10)) < 0
				|| checkStrtolError(&pinWriteTimeout.tv_sec, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing pin write timeout seconds argument\n");
			}
			i++;  // move to second input argumentt
			if (i >= argc
				|| (pinWriteTimeout.tv_nsec = strtol(argv[i], &tempStrtolEndPtr, 10)) < 0
				|| checkStrtolError(&pinWriteTimeout.tv_nsec, argv[i], tempStrtolEndPtr) == ERROR) {
				help();
				error("ERROR processing pin write timeout nanoseconds argument\n");
			}
		} 
		
		 // Motor initalisation CLI processing
		 else if ((strcmp(argv[i], "-sl") == 0) || (strcmp(argv[i], "--step-limit") == 0)) { // step limit argument
			 i++;  // move to input argumentt
			 if (i >= argc
				 || (stepLimit = strtol(argv[i], &tempStrtolEndPtr, 10)) <= 0
				 || checkStrtolError(&stepLimit, argv[i], tempStrtolEndPtr) == ERROR) {
				 help();
				 error("ERROR processing step limit argument\n");
			 }
		 } else if ((strcmp(argv[i], "-m") == 0) || (strcmp(argv[i], "--motor") == 0)) { // pin mode input argument
			motorCount++;

			// add memory for new motor struct
			if ((motors = realloc(motors, sizeof(motor) * motorCount)) == NULL) {
				error("ERROR on reallocating memory for a motor\n");
			}

			// move to direction pin argument
			i++;
			int directionPin;
			if (i >= argc
				|| (directionPin = strtol(argv[i], &tempStrtolEndPtr, 10)) < WIRING_PI_PIN_MIN
				|| directionPin > WIRING_PI_PIN_MAX
				|| (checkStrtolError(&directionPin, argv[i], tempStrtolEndPtr) == ERROR)
				|| pinUsedList[directionPin] == true) {
				help();
				error("ERROR on processing direction pin argument\n");
			}
			motors[motorCount - 1].directionPin = directionPin;
			pinMode(directionPin, OUTPUT);
			pinUsedList[directionPin] = true;

			// move to step pin argument
			i++;  
			int stepPin;
			if (i >= argc
				|| (stepPin = strtol(argv[i], &tempStrtolEndPtr, 10)) < WIRING_PI_PIN_MIN
				|| stepPin > WIRING_PI_PIN_MAX
				|| (checkStrtolError(&stepPin, argv[i], tempStrtolEndPtr) == ERROR)
				|| pinUsedList[stepPin] == true) {
				help();
				error("ERROR on processing step pin argument\n");
			}
			motors[motorCount - 1].stepPin = stepPin;
			pinMode(stepPin, OUTPUT);
			pinUsedList[stepPin] = true;

			// move to micro step 1 pin argument
			i++;
			int microStepOnePin;
			if (i >= argc
				|| (microStepOnePin = strtol(argv[i], &tempStrtolEndPtr, 10)) > WIRING_PI_PIN_MAX
				|| (checkStrtolError(&microStepOnePin, argv[i], tempStrtolEndPtr) == ERROR)
				|| pinUsedList[microStepOnePin] == true) {
				help();
				error("ERROR on processing micro step 1 pin argument\n");
			} else if (microStepOnePin > WIRING_PI_PIN_MIN) {
				pinMode(microStepOnePin, OUTPUT);
				pinUsedList[microStepOnePin] = true;

				// move to micro step 1 pins value argument
				i++;
				int microSpepOnePinValue;
				if (i >= argc
					|| (microSpepOnePinValue = strtol(argv[i], &tempStrtolEndPtr, 10)) < LOW
					|| microSpepOnePinValue > HIGH
					|| (checkStrtolError(&microSpepOnePinValue, argv[i], tempStrtolEndPtr) == ERROR)) {
					help();
					error("ERROR on processing micro step 1 pins value argument\n");
				}
				digitalWrite(microStepOnePin, microSpepOnePinValue);
			}
			

			// move to micro step 2 pin argument
			i++;
			int microStepTwoPin;
			if (i >= argc
				|| (microStepTwoPin = strtol(argv[i], &tempStrtolEndPtr, 10)) > WIRING_PI_PIN_MAX
				|| (checkStrtolError(&microStepTwoPin, argv[i], tempStrtolEndPtr) == ERROR)
				|| pinUsedList[microStepTwoPin] == true) {
				help();
				error("ERROR on processing micro step 2 pin argument\n");
			} else if (microStepTwoPin > WIRING_PI_PIN_MIN) {
				pinMode(microStepTwoPin, OUTPUT);
				pinUsedList[microStepTwoPin] = true;

				// move to micro step 2 pins value argument
				i++;
				int microSpepTwoPinValue;
				if (i >= argc
					|| (microSpepTwoPinValue = strtol(argv[i], &tempStrtolEndPtr, 10)) < LOW
					|| microSpepTwoPinValue > HIGH
					|| (checkStrtolError(&microSpepTwoPinValue, argv[i], tempStrtolEndPtr) == ERROR)) {
					help();
					error("ERROR on processing micro step 2 pins value argument\n");
				}
				digitalWrite(microStepTwoPin, microSpepTwoPinValue);
			}

			// move to step count argument
			i++;
			long stepCount = strtol(argv[i], &tempStrtolEndPtr, 10);
			if (i >= argc || (checkStrtolError(&stepCount, argv[i], tempStrtolEndPtr) == ERROR)) {
				help();
				error("ERROR on processing step count argument\n");
			}
			motors[motorCount - 1].stepCount = stepCount;

			// move to positive step limit argument
			i++;
			long posStepLimit;
			if (i >= argc
				|| (posStepLimit = strtol(argv[i], &tempStrtolEndPtr, 10)) > LONG_MAX // redundant but neetest way to check "i >= argc" first
				|| (checkStrtolError(&posStepLimit, argv[i], tempStrtolEndPtr) == ERROR)) {
				help();
				error("ERROR on processing positive step limit argument\n");
			} 
			motors[motorCount - 1].posStepLimit = posStepLimit;

			// move to negative step limit argument
			i++;
			long negStepLimit;
			if (i >= argc
				|| (negStepLimit = strtol(argv[i], &tempStrtolEndPtr, 10)) < LONG_MIN // redundant but neetest way to check "i >= argc" first
				|| (checkStrtolError(&negStepLimit, argv[i], tempStrtolEndPtr) == ERROR)) {
				help();
				error("ERROR on processing negative step limit argument\n");
			}
			motors[motorCount - 1].negStepLimit = negStepLimit;

			// init motor mutex
			if (pthread_mutex_init(&motors[motorCount - 1].lock, NULL) != 0) {
				error("ERROR initalizing motor mutex");
			}
		} else {
			help();
			error("ERROR argument(s) not recognized\n");
		}
	}
}

/**
  * @breif - prints the help message.
  */
void help() {
	char* msg = "Usage: sudo ./stepperMotorServer [OPTIONS]\n"
		"\n"
		"Example: sudo ./stepperMotorServer -m 0 1 2 1 3 0 0 60 -60\n"
		"\n"
		"[-h] or [--help]\n ~ Displays this message.\n\n"
		"[-p NUM] or [--port NUM]\n ~ Port for the server to listen on.\n\n"
		"[-b NUM] or [--backlog NUM]\n ~ The length of the backlog of unhandled connection.\n\n"
		"[-t NUM] or [--threads NUM]\n ~ Number of threads to used to process connections.\n\n"
		"[-bs NUM] or [--buffer-size NUM]\n ~ Buffer size for every socket.\n\n"
		"[-il NUM] or [--ip-limit NUM]\n ~ The limit of same ip address connections.\n\n"
		"[-pt NUM x 2] or [--ping-timeout NUM x 2]\n ~ How long to wait in seconds and nanoseconds for a response/pong before disconnecting."
												 "\n ~ NOTE: This should allow for the time of any non control frame operations that could occur especially any using timeouts.\n\n"
		"[-it NUM x 2] or [--io-timeout NUM x 2]\n ~ How long to wait in seconds and nanoseconds for a message before giving up and pinging.\n\n"

		// WiringPi help messages
		"[-pwt NUM x 2] or [--pin-write-timeout NUM x 2]\n ~ The minimum amount of time in seconds and nanoseconds to lock a pin for after written to.\n\n"

		// motor help messages
		"[-sl NUM] or [--step-limit NUM]\n ~ The number of steps allowed by one message received. (set 0 for unlimited) \n\n"
		"[-m NUM x 7 to 9] or [-motor NUM x 7 to 9]\n ~ Add a motor to control, NUM's are as follows:"
																"\n ~ Direction pin i.e. The wiringPi defined output pin where 1/HIGH will cause the motor to step in the\n"
																	"\tpositive direction and 0/LOW in the negative.\n"
																"\n ~ Step pin i.e. The wiringPi defined output pin where a rising signal will cause a step in this motor.\n"
																"\n ~ Micro step pin one i.e. The wiringPi defined output pin followed by a bit value e.g. \"7 1\".\n"
																	"\t(set pin negative and no value if not needed)\n"
																"\n ~ Micro step pin two i.e. The wiringPi defined output pin followed by a bit value e.g. \"4 0\".\n"
																	"\t(set pin negative and no value if not needed)\n"
																"\n ~ Step count i.e. Where the motor is compared to the motors zero position. (set 0 if motor is in default position)\n"
																"\n ~ Positive step limit i.e. The most Step count can go in the positive direction. (set negative if not needed)\n"
																"\n ~ Negative step limit i.e. The most Step count can go in the negative direction. (set positive if not needed)\n"
																"\n ~ NOTE: ERROR will be thrown on the same pin used in twice.\n\n"
																"";
	printf("%s", msg);
}

/**
  * @breif - Makes the socket for the server to listen on and returns it.
  * @param - int portNum - The desired port number for the machine.
  * @return - int - Returns the listening server socket.
  */
int makeServerSocket(int portNum) {
	struct sockaddr_in serverAddr;
	int reuseAddr;

	// Set up server/listening socket
	int tempFD = socket(AF_INET, SOCK_STREAM, 0);
	if (tempFD == ERROR) {
		error("ERROR opening socket\n");
	}
	reuseAddr = true;
	if (setsockopt(tempFD, SOL_SOCKET, SO_REUSEADDR, &reuseAddr, sizeof(reuseAddr)) == ERROR) { /* Allows the address to be re used to help stop "Address already in use" */
		error("ERROR on setting SO_REUSEADDR\n");
	}

	// Set up serverAddr
	memset((char*)&serverAddr, 0, sizeof(serverAddr)); /* clear serverAddr */
	serverAddr.sin_family = AF_INET; /* Use Internet domain */
	serverAddr.sin_addr.s_addr = INADDR_ANY; /* Set IP address to machines address */
	serverAddr.sin_port = htons(portNum); /* Set port number */

	// Bind() tempFD to serverAddr to make a socket
	if (bind(tempFD, (struct sockaddr*) &serverAddr, sizeof(serverAddr)) == ERROR) {
		error("ERROR on binding\n");
	}

	if (listen(tempFD, backlog) == ERROR) {
		error("ERROR on Listening\n");
	}

	return tempFD;
}

/**
  * @breif - Prints out an error message.
  * @param - const char* msg - The message to be printed.
  */
void error(const char* MSG) {
	perror(MSG);
	exit(EXIT_FAILURE);
}

/**
  * @breif - given the return value of strtol() or strtoll() will check if an error occured.
  * @param - void* strtolVal - the casted address of the return value from strtol() or strtoll().
  * @param - char* nptr - the c-string strtol() or strtoll() parsed.
  * @param - char* endptr - the c-string set after strtol() or strtoll().
  * @return - int - 0 on success and -1 on error within the strtol() or strtoll() call.
  */
int checkStrtolError(void* strtolVal, char* nptr, char* endptr) {
	long long* tempStrtolVal = (long long*)strtolVal;
	if (((*tempStrtolVal == LONG_MIN) || (*tempStrtolVal == LONG_MAX) || (*tempStrtolVal == LLONG_MAX) || (*tempStrtolVal == LLONG_MAX)) && (errno == ERANGE)) {
		// printf("strtol recieved out of range data.\n");
		return ERROR;
	} else if ((*tempStrtolVal == 0 && errno == EINVAL) || (nptr == endptr)) {
		// printf("strtol didn't recieve any digits.\n");
		return ERROR;
	} else if (errno == EINVAL) {
		// printf("strtol base argument has an unsupported value.\n");
		return ERROR;
	} else {
		return EXIT_SUCCESS;
	}
}

/**
  * @breif - Overrides the interrupt signal to close open sockets and free memory allocated in the heap before exiting.
  */
void interruptHandler() {
	printf("\nExiting Server...\n");

	deallocateThreadPool(&pool, &(sockets.count));
	deallocateSocketList(&sockets);
	deallocateRefSocketList(&connected);
	destroyWiringPi();

	exit(EXIT_SUCCESS);
}

// ~~~~~~~~~~~~~~~~~~~~ socket List ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - Returns an initialized and empty, list of sockets.
  * @return - socketList - The initialized list of sockets.
  */
socketList initSocketList() {
	socketList list;

	list.head = NULL;
	list.tail = NULL;
	pthread_mutex_init(&list.listMutex, NULL);
	sem_init(&list.count, 0, 0);

	return list;
}

/**
  * @breif - Returns an initialized and empty, list of sockets refrences.
  * @return - socketRefList - The initialized list of sockets refrences.
  */
socketRefList initSocketRefList() {
	socketRefList list;

	list.head = NULL;
	pthread_mutex_init(&list.listMutex, NULL);

	return list;
}

/**
  * @breif - Adds a socket to the end of a given socket list.
  * @param - socketList* listPtr - A pointer to the list of sockets to add a new socket to.
  * @param - int socketFD - The socket file descriptor to be added.
  * @param - char* socketIP - socket ip address used to remove from connected list.
  */
void addSocket(socketList* listPtr, int socketFD, char* socketIP, socketRefList* refListPtr) {

  // init new socket node
	socketNode* newNodePtr;
	newNodePtr = malloc(sizeof(socketNode));
	newNodePtr->socketFD = socketFD;
	newNodePtr->doHandshake = true;
	newNodePtr->pingSent = false;
	newNodePtr->socketIP = socketIP;
	newNodePtr->next = NULL;
	newNodePtr->buffer = malloc(sizeof(char) * bufferlen);

	addSocketRef(refListPtr, socketIP);

	pthread_mutex_lock(&(listPtr->listMutex));

	if (listPtr->head == NULL) {
		listPtr->head = newNodePtr;
		listPtr->tail = newNodePtr;
	} else if (listPtr->tail != NULL) {
		listPtr->tail->next = newNodePtr;
		listPtr->tail = newNodePtr;
	} else {
		error("No Socket list tail\n");
	}

	pthread_mutex_unlock(&(listPtr->listMutex));

	sem_post(&(listPtr->count));
}

/**
  * @breif - Adds a socket refrence to the end of the given socket refrence list.
  * @param - socketRefList* listPtr - A pointer to the list of sockets refrences to add a new socket refrence to.
  * @param - char* socketIP - the ip of the connecting socket, used to prevent a DoS attack.
  */
void addSocketRef(socketRefList* listPtr, char* socketIP) {

	// init new socket node
	socketRefNode* newNodePtr;
	newNodePtr = malloc(sizeof(socketRefNode));
	newNodePtr->socketIP = socketIP;
	newNodePtr->next = NULL;

	pthread_mutex_lock(&(listPtr->listMutex));

	if (listPtr->head == NULL) {
		listPtr->head = newNodePtr;
	} else {

		// find last node from head and add node there
		socketRefNode* currentNode = listPtr->head;
		while (currentNode->next != NULL) {
			currentNode = currentNode->next;
		}
		currentNode->next = newNodePtr;
	}

	pthread_mutex_unlock(&(listPtr->listMutex));
}

/**
  * @breif - Returns and removes a socket node from the list of sockets (Method waits on a semaphore).
  * @param - socketList* listPtr - A pointer to the list to retrieve a socket from.
  * @param - threadPool* poolPtr - A pointer to the pool in whitch this method is running.
  * @return - socketNode* - The socketNode that has been taken from the list or null if thread is exiting.
  */
socketNode* takeSocket(socketList* listPtr, threadPool* poolPtr) {
	struct socketNode* socketPtr = NULL;

	sem_wait(&(listPtr->count));

	// if threads are trying to exit after waiting return early
	if (poolPtr->killThreads) {
		return NULL;
	}

	// Remove node from list
	pthread_mutex_lock(&(listPtr->listMutex));

	if (listPtr->head != NULL) {
		socketPtr = listPtr->head;
		if (listPtr->head->next != NULL) {
			listPtr->head = listPtr->head->next;
		} else {
			listPtr->head = NULL;
			listPtr->tail = NULL;
		}
	} else {
		error("ERROR List is empty\n");
	}

	// clear pointer
	socketPtr->next = NULL;

	pthread_mutex_unlock(&(listPtr->listMutex));

	// check if socket node has a valid file discriptor
	if (socketPtr->socketFD == 0) {
		error("ERROR with socket file discriptor\n");
	}

	return socketPtr;
}

/**
  * @breif - Adds a socket to the end of a given socket list.
  * @param - socketNode* listPtr - A pointer to the list of sockets to return the socket to.
  * @param - socketNode* socket - The socket to be returned.
  */
void returnSocket(socketList* listPtr, socketNode* socketPtr) {
	pthread_mutex_lock(&(listPtr->listMutex));

	if (listPtr->head == NULL) {
		listPtr->head = socketPtr;
		listPtr->tail = socketPtr;
	} else if (listPtr->tail != NULL) {
		listPtr->tail->next = socketPtr;
		listPtr->tail = socketPtr;
	} else {
		error("No Socket list tail\n");
	}

	pthread_mutex_unlock(&(listPtr->listMutex));

	sem_post(&(listPtr->count));
}

/**
  * @breif - Closes the given socket, frees the malloc for the node and removes the socket refrence (Node must be removed from the socket list).
  * @param - socketNode* socket - The socket to be closed and freed.
  * @param - socketRefList* refList - the list the socket refrences are stored in.
  */
void closeSocket(socketNode* socketPtr, socketRefList* refList) {
	bool removedRef = false;
	char* socketIP;

	// naive check
	if (socketPtr->next != NULL) {
		error("ERROR on closing socket, socket might still be in the socket list\n");
	}

	removeSocketRefrence(socketPtr, refList);
	socketIP = socketPtr->socketIP;

	// close socket and deallocate
	close(socketPtr->socketFD);
	free(socketPtr->buffer);
	free(socketPtr);
	printf("\n%s Disconnected\n", socketIP);
}

/**
  * @breif - removes the refrence for the given socket.
  * @param - socketNode* socket - The socket to being closed and needs the refrence removed.
  * @param - socketRefList* refList - the list of socket refrences to remove from.
  */
void removeSocketRefrence(socketNode* socketPtr, socketRefList* refList) {
	socketRefNode* currSocketRefPtr = refList->head;
	socketRefNode* socketRefPtr;

	if (currSocketRefPtr == NULL) {
		error("ERROR on removing socket refrence list is empty\n");
	}

	// if ref node is the head
	if (strcmp(currSocketRefPtr->socketIP, socketPtr->socketIP) == 0) {
		socketRefPtr = currSocketRefPtr->next;
		free(refList->head);
		refList->head = socketRefPtr;
		return;
	}

	// otherwise find it elsewhere
	socketRefPtr = currSocketRefPtr; // aka previous one from now on
	currSocketRefPtr = currSocketRefPtr->next;
	do {
		if (strcmp(currSocketRefPtr->socketIP, socketPtr->socketIP) == 0) {
			socketRefPtr->next = currSocketRefPtr->next; // close list
			free(currSocketRefPtr);
			return;
		}
		socketRefPtr = currSocketRefPtr;
		currSocketRefPtr = currSocketRefPtr->next;
	} while (currSocketRefPtr != NULL);

	error("ERROR on removing socket refrence, could not be found\n");
}

/**
  * @breif - counts the amount of times the current ip has connects and returns true if greater than connectLimit false otherwise.
  * @param - char* socketIP - the ip address to be tested.
  * @param - socketRefList* refListPtr - the list of socket refrences, aka all sockets connected.
  * @param - int connectLimit - the amount of times the same ip can connect but not exceede.
  * @return - bool - weather the given socket had connected too many times or not.
  */
bool connectedTooManyTimes(char* socketIP, socketRefList* refListPtr, int connectLimit) {
	int timesConnected = 0;
	socketRefNode* currRefNodePtr = refListPtr->head;
	while (currRefNodePtr != NULL) {
		if (strcmp(currRefNodePtr->socketIP, socketIP) == 0) {
			timesConnected++;
		}
		if (timesConnected >= connectLimit) {
			return true;
		}
		currRefNodePtr = currRefNodePtr->next;
	}
	return false;
}

/**
  * @breif - Deallocates all memory held by the given socket list (list must be complete e.g no socket used by a thread ideally threads not running).
  * @param - socketList* listPtr - A pointer to the list of sockets to be deleted or deallocated.
  */
void deallocateSocketList(socketList* listPtr) {
	int destroyedSocketCount = 0;
	pthread_mutex_lock(&(listPtr->listMutex));
	if (listPtr->head != NULL && listPtr->tail != NULL) {

		//Free all the nodes in the linked list
		for (socketNode* currentNode = listPtr->head; currentNode != NULL; currentNode = currentNode->next) {
			close(currentNode->socketFD);
			free(currentNode->buffer);
			free(currentNode);
			destroyedSocketCount++;
		}
	} else if (listPtr->head == NULL && listPtr->tail == NULL) {
		// empty list allowed
		// do nothing
	} else {
		error("ERROR on deallocation of socket list, list head or tail missing\n");
	}
	pthread_mutex_unlock(&(listPtr->listMutex));

	pthread_mutex_destroy(&(listPtr->listMutex));
	sem_destroy(&(listPtr->count));
	printf("\n%i Sockets Destroyed", destroyedSocketCount);
}

/**
  * @breif - Deallocates the socket refrence list.
  * @param - socketList* listPtr - A pointer to the socket refrence list to be deleted or deallocated.
  */
void deallocateRefSocketList(socketRefList* listPtr) {
	int destroyedSocketRefCount = 0;
	pthread_mutex_lock(&(listPtr->listMutex));
	if (listPtr->head != NULL) {

		//Free all the nodes in the linked list
		for (socketRefNode* currentNode = listPtr->head; currentNode != NULL; currentNode = currentNode->next) {
			free(currentNode);
			destroyedSocketRefCount++;
		}
	}
	pthread_mutex_unlock(&(listPtr->listMutex));

	pthread_mutex_destroy(&(listPtr->listMutex));
	printf("\n%i Socket Refrences Destroyed\n", destroyedSocketRefCount);
}

// ~~~~~~~~~~~~~~~~~~~~ Thread pool ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - Returns an initialized a thread pool (initialized without threads).
  * @return - threadPool - The initialized thread pool.
  */
threadPool initThreadPool() {
	threadPool pool;

	pool.head = NULL;
	pool.killThreads = false;

	return pool;
}

/**
  * @breif - Takes a socket when available and and processes it.
  */
void* processSocket(void* voidPtr) {
	socketNode* socketPtr;

	while (pool.killThreads == false) {
		socketPtr = takeSocket(&sockets, &pool);

		// post wait check
		if (pool.killThreads) {
			if (socketPtr != NULL) { // if we have this malloc don't loose it
				returnSocket(&sockets, socketPtr);
			}
			break;
		}

		// Handshake
		if (socketPtr->doHandshake) {
			if (handshake() == ERROR) {
				closeSocket(socketPtr, &connected);
				continue;
			} else {
				socketPtr->doHandshake = false;
			}
		} else { // read and handel data
			bool isBinary;
			bool pongRecieved;
			long long int msgLen;

			// read data
			if ((msgLen = readWebSocketData(socketPtr, &isBinary)) == ERROR) {
				closeSocket(socketPtr, &connected);
				continue;
			} else if (msgLen > 0) {
				if (parseWiringPiBuffer(socketPtr) == ERROR) {
					closeSocket(socketPtr, &connected);
					continue;
				}
			}
		}
		returnSocket(&sockets, socketPtr);
	}
	return EXIT_SUCCESS;
}

/**
  * @breif - Adds a thread to the given thread pool
  * @param - threadPool* poolPtr - A pointer to the pool to add a new thread to.
  */
void addThread(threadPool* poolPtr) {
	threadNode* newThrdPtr;

	// Set up new thread node
	newThrdPtr = malloc(sizeof(threadNode));
	newThrdPtr->next = NULL;
	newThrdPtr->threadId = pthread_self();
	pthread_create(&newThrdPtr->threadId, NULL, &processSocket, (void*)NULL);

	if (poolPtr->head == NULL) {
		poolPtr->head = newThrdPtr;
	} else {

	  // find last node from head and add node there
		threadNode* currentNode = poolPtr->head;
		while (currentNode->next != NULL) {
			currentNode = currentNode->next;
		}
		currentNode->next = newThrdPtr;
	}
}

/**
  * @breif - Deallocates all memory held by the given pool and ends the threads.
  * @param - threadPool* poolPtr - The pointer to the thread pool to be deleted or deallocated.
  * @param - sem_t* semPtr - The pointer to the semaphore the threads will be waiting on.
  */
void deallocateThreadPool(threadPool* poolPtr, sem_t* semPtr) {
	int destroyedThreadCount = 0;

	if (poolPtr->head != NULL) {
		poolPtr->killThreads = true;

		// stop threads from waiting 
		for (threadNode* currentNode = poolPtr->head; currentNode != NULL; currentNode = currentNode->next) {
			sem_post(semPtr);
		}

		//Free all the threadNodes in the linked list as their threads finish execution
		for (threadNode* currentNode = poolPtr->head; currentNode != NULL; currentNode = currentNode->next) {
			printf("\njoining thread %i", currentNode->threadId);
			if (pthread_join(currentNode->threadId, NULL) != 0) { // Wait for thread to terminate
				error("Error on joining thread\n");
			}
			free(currentNode);
			destroyedThreadCount++;
		}
	} else {
		error("ERROR on deallocation of thread pool, pool has no threads\n");
	}

	printf("\n\n%i Threads Destroyed", destroyedThreadCount);
}

// ~~~~~~~~~~~~~~~~~~~~ WebSockets ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - preforms the Websockets Handshake as specified by v13
  * @return - int - returns 0 on succesful Handshake and -1 on error or rejected
  */
int handshake(socketNode* socketPtr) {

	// setup for select so read can timeout
	fd_set readFDs;
	FD_ZERO(&readFDs);
	FD_SET(socketPtr->socketFD, &readFDs);

	// check if read is ready until timeout 
	int fdsReady = pselect(socketPtr->socketFD + 1, &readFDs, NULL, NULL, &ioTimeout, NULL);
	if (fdsReady == ERROR) {
		return ERROR;
	} else if (fdsReady == 1) {
		if (read(socketPtr->socketFD, socketPtr->buffer, bufferlen) == ERROR) {
			return ERROR;
		}
	} else { // timed out because only 1 FD in sets
		return ERROR;
	}

	char* currentLine;
	char* subCurrLine;
	char* currBuffTok;
	const char* CRLF = "\r\n";
	const char* SP = " ";
	const char* FWDS = "/";

	currBuffTok = strtok_r(socketPtr->buffer, CRLF, &currentLine); // break buffer by new lines and return the Request-Line

	currBuffTok = strtok_r(currBuffTok, SP, &subCurrLine); // break the Request-Line and return the Method 

	// reject if it is not a GET method
	if (strcmp(currBuffTok, "GET")) {
		badRequestResponse(socketPtr, NULL);
		return ERROR;
	}

	currBuffTok = strtok_r(NULL, SP, &subCurrLine); // return HTTP Request-URI (not used here)
	currBuffTok = strtok_r(NULL, FWDS, &subCurrLine); // break the HTTP-Version by forward slash and return "HTTP"
	currBuffTok = strtok_r(NULL, FWDS, &subCurrLine); // return version number

	// reject if version is less than 1.1
	if (strtof(currBuffTok, NULL) < 1.1) {
		badRequestResponse(socketPtr, NULL);
		return ERROR;
	}

	// Request-Line handled start with headders if any
	currBuffTok = strtok_r(NULL, CRLF, &currentLine);

	char* wsKey;
	const char* HEAD_DELIM = ":";

	while (currBuffTok != NULL) {

		currBuffTok = strtok_r(currBuffTok, HEAD_DELIM, &subCurrLine); // break current headder into key, value pair (value may be more than one delimiter)

		// strcmp(strtok_r(NULL, SP, &subCurrLine) to return value without leading spaces
		// TODO: check origin/host
		if (strcmp(currBuffTok, "Connection") == 0) {
			if (strcmp(strtok_r(NULL, SP, &subCurrLine), "Upgrade")) {
				badRequestResponse(socketPtr, NULL);
				return ERROR;
			}
		} else if (strcmp(currBuffTok, "Upgrade") == 0) {
			if (strcmp(strtok_r(NULL, SP, &subCurrLine), "websocket")) {
				badRequestResponse(socketPtr, NULL);
				return ERROR;
			}
		} else if (strcmp(currBuffTok, "Sec-WebSocket-Key") == 0) {
			wsKey = strtok_r(NULL, SP, &subCurrLine);
		} else if (strcmp(currBuffTok, "Sec-WebSocket-Version") == 0) {
			if (strtof(strtok_r(NULL, SP, &subCurrLine), NULL) != WS_VERSION) {
				badRequestResponse(socketPtr, "Sec-WebSocket-Version: 13\r\n");
				return ERROR;
			}
		}
		currBuffTok = strtok_r(NULL, CRLF, &currentLine); // next line
	}

	// if reacher request looks alright so send accept response
	return acceptWebSocketResponse(socketPtr, wsKey);
}

/**
  * @breif - cyphers key and sends a standard accepted websocket upgrade response.
  * @param - socketNode* socketPtr - the socket to send the response to.
  * @param - char* wsKey - the websocket key used to unlock websocket functionality.
  * @return - int - 0 on success, -1 on an error (buffer too small or could not write to socket).
  */
int acceptWebSocketResponse(socketNode* socketPtr, char* wsKey) {
	char* statusLine = "HTTP/1.1 101 Switching Protocols\r\n"
		"Upgrade: websocket\r\n"
		"Connection: Upgrade\r\n";
	char* acceptCodeFieldName = "Sec-WebSocket-Accept: ";
	char* endOfResponse = "\r\n\r\n";

	// cypher key

	// concatenate key and secret
	const char* SECRET = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
	int keySecretlen = strlen(wsKey) + strlen(SECRET);
	bzero(socketPtr->buffer, sizeof(char) * keySecretlen); // clear buffer for key and secret
	strcat(socketPtr->buffer, wsKey);
	strcat(socketPtr->buffer, SECRET);

	// get hash (SHA-1) of keyAndSecret
	unsigned char hash[SHA_DIGEST_LENGTH];
	SHA1(socketPtr->buffer, keySecretlen, hash);

	// encode to base64 and store in buffer
	bzero(socketPtr->buffer, sizeof(char) * bufferlen); // clear buffer completely
	if (base64encode(hash, SHA_DIGEST_LENGTH, socketPtr->buffer, bufferlen) == ERROR) {
		return ERROR;
	}

	// socket buffer in use so we'll make one just the right size
	int responseLen = strlen(statusLine) + strlen(acceptCodeFieldName) + strlen(socketPtr->buffer) + strlen(endOfResponse);
	char* response = calloc(responseLen, sizeof(char));

	// compile response
	strcat(response, statusLine);
	strcat(response, acceptCodeFieldName);
	strcat(response, socketPtr->buffer);
	strcat(response, endOfResponse);

	// setup for select so write can timeout
	fd_set writeFDs;
	FD_ZERO(&writeFDs);
	FD_SET(socketPtr->socketFD, &writeFDs);

	// check if read is ready until timeout 
	int fdsReady = pselect(socketPtr->socketFD + 1, NULL, &writeFDs, NULL, &ioTimeout, NULL);
	if (fdsReady == ERROR) {
		return ERROR;
	} else if (fdsReady == 1) {
		if (write(socketPtr->socketFD, response, responseLen) == ERROR) {
			return ERROR;
		}
	} else { // timed out because there is only 1 socket fd available
		return ERROR;
	}

	free(response);
	return EXIT_SUCCESS;
}

/**
  * @breif - Reads any frames recieved from the given socket and fills its buffer with any recieved data.
  * @param - socketNode* socketPtr - the socket to read and save any recieved data to.
  * @param - bool* isBinary - a flag indicating weather the returned data is binary or text.
  * @return - long long int - 0 on controll frames handled or no payload, -1 on error or socket closing and greater than 0 identifying the data length.
  */
long long int readWebSocketData(socketNode* socketPtr, bool* isBinary) {
	bool finished = false;
	unsigned short int opCode = WS_OP_CODE_INVALID;
	unsigned long long int length = 0;
	char* payload = NULL;


	// read and concatinate frames until finished
	while (!finished) {
		unsigned long long int frameLength = 0;
		char* framePayload = NULL;

		// clear whole buffer because we won't know much reading will fill
		bzero(socketPtr->buffer, sizeof(char) * bufferlen);

		if ((frameLength = readWebSocketFrame(socketPtr, &framePayload, &finished, &opCode)) == ERROR) {
			return ERROR;
		}

		// if ping was sent
		if (opCode == WS_OP_CODE_INVALID) {
			if (frameLength == 0) {

				// need to check because client may take to long to send fragmented message/pong
				// ping sent if not already to check that it has been too long since one was sent
				struct timespec pongTime;
				clock_gettime(CLOCK_REALTIME, &pongTime);
				struct timespec timeTaken = timespecDiff(socketPtr->pingTime, pongTime);

				// if taken/taking too long close socket
				if (timeTaken.tv_sec > pingTimeout.tv_sec || timeTaken.tv_nsec > pingTimeout.tv_nsec) {
					if (framePayload != NULL) {
						free(framePayload);
					}
					if (payload != NULL) {
						free(payload);
					}
					printf("%s took too long to respond (%ld.%06lds) disconnecting...\n", socketPtr->socketIP, timeTaken.tv_sec, timeTaken.tv_nsec);
					return ERROR;
				}

				if (payload != NULL) {
					finished = false; // continue current fragmented message
				} else {
					return WS_CONTROLL_FRAME_RETURN;
				}
			} else { // something went wrong
				if (framePayload != NULL) {
					free(framePayload);
				}
				if (payload != NULL) {
					free(payload);
				}
				return ERROR;
			}
		} else if ((WS_OP_CODE_CONTROLL_FRAME & opCode) == WS_OP_CODE_CONTROLL_FRAME) {
			if (opCode == WS_OP_CODE_CLOSE) {
				if (framePayload != NULL) {
					writeWebSocketFrame(socketPtr, &framePayload, frameLength, false, finished, opCode); // echo recieved close frame
					free(framePayload);
				} else {
					char* empty = "";
					writeWebSocketFrame(socketPtr, &empty, 0, false, finished, opCode);
				}
				if (payload != NULL) {
					free(payload);
				}
				return ERROR;
			} else if (opCode == WS_OP_CODE_PING) {
				if (!finished) {  // controll frames must have a set fin bit
					if (framePayload != NULL) {
						free(framePayload);
					}
					if (payload != NULL) {
						free(payload);
					}
					writeWebSocketCloseFrame(socketPtr, 1002, "Incorrect controll frame format");
					return ERROR;
				}

				// send pong
				writeWebSocketFrame(socketPtr, &framePayload, frameLength, false, true, WS_OP_CODE_PONG);

				if (payload != NULL) {
					finished = false; // continue current fragmented message
				} else {
					free(framePayload);
					return WS_CONTROLL_FRAME_RETURN;
				}
			} else if (opCode == WS_OP_CODE_PONG) {
				if (!finished) { // controll frames must have a set fin bit
					if (framePayload != NULL) {
						free(framePayload);
					}
					if (payload != NULL) {
						free(payload);
					}
					writeWebSocketCloseFrame(socketPtr, 1002, "Incorrect controll frame format");
					return ERROR;
				}

				// check ping result
				if (socketPtr->pingSent) {
					struct timespec pongTime;
					clock_gettime(CLOCK_REALTIME, &pongTime);
					struct timespec timeTaken = timespecDiff(socketPtr->pingTime, pongTime);

					// if ping complete clear reset for a new ping
					if (memcmp(socketPtr->pingPayload, framePayload, DEF_PING_RAND_BYTES_COUNT) == 0) {
						bzero(socketPtr->pingPayload, DEF_PING_RAND_BYTES_COUNT);
						socketPtr->pingSent = false;
						// printf("Ping: %ld.%06ld\n", timeTaken.tv_sec, timeTaken.tv_nsec); // print ping time 
					}

					// if taken/taking too long close socket
					if (timeTaken.tv_sec > pingTimeout.tv_sec || timeTaken.tv_nsec > pingTimeout.tv_nsec) {
						if (framePayload != NULL) {
							free(framePayload);
						}
						if (payload != NULL) {
							free(payload);
						}
						printf("%s took too long to respond (%ld.%06lds) disconnecting...\n", socketPtr->socketIP, timeTaken.tv_sec, timeTaken.tv_nsec);
						return ERROR;
					}
				}

				if (payload != NULL) {
					finished = false; // continue current fragmented message
				} else {
					free(framePayload);
					return WS_CONTROLL_FRAME_RETURN;
				}
			}
		} else {
			if (opCode == WS_OP_CODE_BINARY) {
				*isBinary = true;
			} else if (opCode == WS_OP_CODE_TEXT) {
				*isBinary = false;
			}

			// store framePayload or concatenate to payload if not the first itteration
			if (payload == NULL) { // first frame

				// give up if payload won't fit in buffer
				if ((frameLength) > bufferlen) {
					writeWebSocketCloseFrame(socketPtr, 1009, "Buffer Overflow, send smaller messages");
					if (framePayload != NULL) {
						free(framePayload);
					}
					if (payload != NULL) {
						free(payload);
					}
					return ERROR;
				}

				if ((payload = calloc(frameLength, sizeof(char))) == NULL) {
					if (framePayload != NULL) {
						free(framePayload);
					}
					if (payload != NULL) {
						free(payload);
					}
					return ERROR;
				}
				strncpy(payload, framePayload, frameLength * sizeof(char));
				length = frameLength;
			} else {

				// give up if payload won't fit in buffer
				if ((length + frameLength) > bufferlen) {
					writeWebSocketCloseFrame(socketPtr, 1009, "Buffer Overflow, send smaller messages");
					if (framePayload != NULL) {
						free(framePayload);
					}
					if (payload != NULL) {
						free(payload);
					}
					return ERROR;
				}

				if (realloc(payload, sizeof(char) * (length + frameLength)) == NULL) {
					if (framePayload != NULL) {
						free(framePayload);
					}
					if (payload != NULL) {
						free(payload);
					}
					return ERROR;
				}

				// copy in new payload after length
				char* framePayloadStart = &payload[length + 1];
				memcpy(framePayloadStart, framePayload, sizeof(char) * frameLength);
				length += frameLength;
			}
		}

		free(framePayload);
	}

	// clear whole buffer for sanitization sake 
	bzero(socketPtr->buffer, sizeof(char) * bufferlen);

	// fill buffer and free payload
	memcpy(socketPtr->buffer, payload, sizeof(char) * length);
	free(payload);

	return length;
}

/**
  * @breif - reads/decodes a websocket frame and stores extracted data.
  * @param - socketNode* socketPtr - the socket to read the frame from.
  * @param - char** payload - where the decoded payload will be stored.
  * @param - bool* finished - where weather it is a finish frame or not will be stored.
  * @param - unsigned short int* opCode - where the opperation code will be stored.
  * @return - long long int - 0 on unresponsive and ping sent, -1 on an error and the length of the payload otherwise.
  */
long long int readWebSocketFrame(socketNode* socketPtr, char** payload, bool* finished, unsigned short int* opCode) {
	bool masked;
	short int currBitVal = 0;
	short int currByteOffset = 0;
	unsigned short int extLenCode = 0;
	unsigned long long int bytesRead = bufferlen;
	unsigned long long int length = 0;
	char mask[WS_MASK_SIZE_BYTES] = {0};

	// setup for select so read can timeout
	fd_set readFDs;
	FD_ZERO(&readFDs);
	FD_SET(socketPtr->socketFD, &readFDs);

	// check if read is ready until timeout 
	int fdsReady = pselect(socketPtr->socketFD + 1, &readFDs, NULL, NULL, &ioTimeout, NULL);
	if (fdsReady == ERROR) {
		return ERROR;
	} else if (fdsReady == 1) {
		if (read(socketPtr->socketFD, socketPtr->buffer, bufferlen) == ERROR) {
			return ERROR;
		}
	} else { // timed out because only 1 FD in sets
		return writeWebSocketPingFrame(socketPtr);
	}

	currByteOffset = WS_INFO_BYTE;
	*finished = (bool)((socketPtr->buffer[currByteOffset] >> WS_FIN_BIT) & 0x01);

	// opCode
	*opCode = 0; // clear opCode
	for (short int bit = WS_OP_CODE_START; bit >= WS_OP_CODE_END; bit--) {
		currBitVal = (socketPtr->buffer[currByteOffset] >> bit) & 0x01;
		*opCode ^= (-currBitVal ^ *opCode) & (1 << bit);
	}

	currByteOffset = WS_MASK_DEF_LEN_BYTE;
	masked = (bool)((socketPtr->buffer[currByteOffset] >> WS_MASK_BIT) & 0x01);

	// extLenCode and default length
	for (short int bit = WS_DEF_LEN_BIT_START; bit >= WS_DEF_LEN_BIT_END; bit--) {
		currBitVal = (socketPtr->buffer[currByteOffset] >> bit) & 0x01;
		extLenCode ^= (-currBitVal ^ extLenCode) & (1 << bit);
		length = extLenCode;
	}

	// extendet length
	if (extLenCode == WS_LEN_EXT1) {
		length = 0;
		currByteOffset = WS_LEN_EXT_BYTE_START;
		for (int byte = currByteOffset; byte <= WS_LEN_EXT1_BYTE_END; byte++) {
			short int offset = ((WS_LEN_EXT1_BYTE_LEN - 1) - (byte - currByteOffset)) * BITS_IN_A_BYTE;
			length |= (socketPtr->buffer[byte] << offset);
		}
		currByteOffset += WS_LEN_EXT1_BYTE_LEN; // mask or payload start
	} else if (extLenCode == WS_LEN_EXT2) {
		length = 0;
		currByteOffset = WS_LEN_EXT_BYTE_START;
		if (((socketPtr->buffer[currByteOffset] >> MOST_SIGNIFICANT_BIT) & 0x01) != 0) { // most significant bit must be 0
			writeWebSocketCloseFrame(socketPtr, 1002, "Incorrect Extended length of type 127");
			return ERROR;
		}
		for (int byte = currByteOffset; byte <= WS_LEN_EXT2_BYTE_END; byte++) {
			short int offset = ((WS_LEN_EXT2_BYTE_LEN - 1) - (byte - currByteOffset)) * BITS_IN_A_BYTE;
			length |= (socketPtr->buffer[byte] << offset);
		}
		currByteOffset += WS_LEN_EXT2_BYTE_LEN; // mask or payload start
	} else {
		currByteOffset++; // mask or payload start
	}

	// store mask
	if (masked) {
		for (int byte = currByteOffset; byte < currByteOffset + WS_MASK_SIZE_BYTES; byte++) { // '<' because (currByteOffset + WS_MASK_SIZE_BYTES) is payload start
			short int offset = ((WS_MASK_SIZE_BYTES - 1) - (byte - currByteOffset)) * BITS_IN_A_BYTE;
			mask[byte - currByteOffset] = socketPtr->buffer[byte];
		}
		currByteOffset += WS_MASK_SIZE_BYTES;
	}

	if ((currByteOffset + length) - 1 > bytesRead) {
		writeWebSocketCloseFrame(socketPtr, 1009, "Buffer Overflow, send smaller messages");
		return ERROR;
	}

	if ((*payload) != NULL) {
		free((*payload));
		(*payload) = NULL;
	}

	// because buffer is being used make payload buffer
	if (((*payload) = malloc(sizeof(char) * length)) == NULL) {
		return ERROR;
	}

	// TODO: make pointer to current point in buffer to avoid unsigned long long int overflow
	// store payload
	if (masked) {
		for (unsigned long long int byte = currByteOffset; byte < currByteOffset + length; byte++) { // '<' because (currByteOffset + length) is 1 too many
			(*payload)[byte - currByteOffset] = socketPtr->buffer[byte] ^ mask[(byte - currByteOffset) % 4];
		}
	} else {
		for (unsigned long long int byte = currByteOffset; byte < currByteOffset + length; byte++) { // '<' because (currByteOffset + length) is 1 too many
			(*payload)[byte - currByteOffset] = socketPtr->buffer[byte];
		}
	}

	return length;
}

/**
  * @breif - encodes/writes a websocket frame with the given data.
  * @param - socketNode* socketPtr - the socket to write the frame to.
  * @param - char** payload - the payload to be sent.
  * @param - unsigned long long int payloadLen - the length of the given payload.
  * @param - bool masked - weather to mask the frame payload or not.
  * @param - bool finished - weather it is a finish/controll frame or not.
  * @param - unsigned short int opCode - the opperation code for the frame.
  * @return - int - 0 on success or -1 on an error.
  */
int writeWebSocketFrame(socketNode* socketPtr, char** payload, unsigned long long int payloadLen, bool masked, bool finished, unsigned short int opCode) {
	short int currBitVal = 0;
	short int currByteOffset = 0;
	unsigned short int extLenCode = 0;
	unsigned long long int msgLen = 0;
	char mask[WS_MASK_SIZE_BYTES] = {0};

	// information error checking
	if (opCode > 0xF) {
		return ERROR;
	}
	if (!finished && ((WS_OP_CODE_CONTROLL_FRAME & opCode) == WS_OP_CODE_CONTROLL_FRAME)) {
		return ERROR;
	}
	if (((WS_OP_CODE_CONTROLL_FRAME & opCode) == WS_OP_CODE_CONTROLL_FRAME) && payloadLen > WS_DEFAULT_FRAME_MAX_LEN) {
		return ERROR;
	}

	// buffer overflow checking
	if (payloadLen <= WS_DEFAULT_FRAME_MAX_LEN) {
		if ((msgLen = WS_MIN_FRAME_SIZE_BYTES + ((int)masked * WS_MASK_SIZE_BYTES) + payloadLen) > bufferlen) {
			return ERROR;
		}
	} else {
		if (payloadLen > WS_LEN_EXT1_MAX) {
			if ((msgLen = WS_MIN_FRAME_SIZE_BYTES + WS_LEN_EXT2_BYTE_LEN + ((int)masked * WS_MASK_SIZE_BYTES) + payloadLen) > bufferlen) {
				return ERROR;
			}
		} else {
			if ((msgLen = WS_MIN_FRAME_SIZE_BYTES + WS_LEN_EXT1_BYTE_LEN + ((int)masked * WS_MASK_SIZE_BYTES) + payloadLen) > bufferlen) {
				return ERROR;
			}
		}
	}

	bzero(socketPtr->buffer, bufferlen * sizeof(char));

	currByteOffset = WS_INFO_BYTE;
	socketPtr->buffer[currByteOffset] ^= (-((unsigned short)finished) ^ socketPtr->buffer[currByteOffset]) & (1 << WS_FIN_BIT);

	// opCode
	for (short int bit = WS_OP_CODE_START; bit >= WS_OP_CODE_END; bit--) {
		currBitVal = (opCode >> bit) & 0x01;
		socketPtr->buffer[currByteOffset] ^= (-currBitVal ^ socketPtr->buffer[currByteOffset]) & (1 << bit);
	}

	currByteOffset = WS_MASK_DEF_LEN_BYTE;
	socketPtr->buffer[currByteOffset] ^= (-((unsigned short)masked) ^ socketPtr->buffer[currByteOffset]) & (1 << WS_MASK_BIT);

	if (payloadLen <= WS_DEFAULT_FRAME_MAX_LEN) {

		// set default length
		for (short int bit = WS_DEF_LEN_BIT_START; bit >= WS_DEF_LEN_BIT_END; bit--) {
			currBitVal = (payloadLen >> bit) & 0x01;
			socketPtr->buffer[currByteOffset] ^= (-currBitVal ^ socketPtr->buffer[currByteOffset]) & (1 << bit);
		}

		currByteOffset++; // mask or payload start
	} else {
		if (payloadLen > WS_LEN_EXT1_MAX) {

			// set extended length 2 code
			extLenCode == WS_LEN_EXT2;
			for (short int bit = WS_DEF_LEN_BIT_START; bit >= WS_DEF_LEN_BIT_END; bit--) {
				currBitVal = (extLenCode >> bit) & 0x01;
				socketPtr->buffer[currByteOffset] ^= (-currBitVal ^ socketPtr->buffer[currByteOffset]) & (1 << bit);
			}

			// set extended 2 length
			currByteOffset = WS_LEN_EXT_BYTE_START;
			if (((payloadLen >> (WS_LEN_EXT2_BYTE_LEN * BITS_IN_A_BYTE) - 1) & 0x01) != 0) { // most significant bit must be 0
				return ERROR;
			}
			for (int byte = currByteOffset; byte <= WS_LEN_EXT2_BYTE_END; byte++) {
				for (short int bit = 7; bit >= 0; bit--) { // for each bit
					short int offset = ((WS_LEN_EXT2_BYTE_LEN - 1) - (byte - currByteOffset)) * BITS_IN_A_BYTE + bit;
					currBitVal = (payloadLen >> offset) & 0x01;
					socketPtr->buffer[currByteOffset] ^= (-currBitVal ^ socketPtr->buffer[currByteOffset]) & (1 << bit);
				}
			}

			currByteOffset += WS_LEN_EXT2_BYTE_LEN; // mask or payload start
		} else {

			// set extended length 1 code
			extLenCode = WS_LEN_EXT1;
			for (short int bit = WS_DEF_LEN_BIT_START; bit >= WS_DEF_LEN_BIT_END; bit--) {
				currBitVal = (extLenCode >> bit) & 0x01;
				socketPtr->buffer[currByteOffset] ^= (-currBitVal ^ socketPtr->buffer[currByteOffset]) & (1 << bit);
			}

			// set extended 1 length
			currByteOffset = WS_LEN_EXT_BYTE_START;
			for (int byte = currByteOffset; byte <= WS_LEN_EXT1_BYTE_END; byte++) {
				for (short int bit = 7; bit >= 0; bit--) { // for each bit
					short int offset = ((WS_LEN_EXT1_BYTE_LEN - 1) - (byte - currByteOffset)) * BITS_IN_A_BYTE + bit;
					currBitVal = (payloadLen >> offset) & 0x01;
					socketPtr->buffer[currByteOffset] ^= (-currBitVal ^ socketPtr->buffer[currByteOffset]) & (1 << bit);
				}
			}

			currByteOffset += WS_LEN_EXT1_BYTE_LEN; // mask or payload start
		}
	}

	// set mask
	if (masked) {
		for (int byte = currByteOffset; byte < currByteOffset + WS_MASK_SIZE_BYTES; byte++) { // '<' because (currByteOffset + WS_MASK_SIZE_BYTES) is payload start
			char randByte;

			// generates 1 random byte
			if (RAND_bytes(&randByte, 1) != 1) {
				return ERROR;
			}

			socketPtr->buffer[byte] = randByte;
			mask[byte - currByteOffset] = randByte;
		}
		currByteOffset += WS_MASK_SIZE_BYTES;
	}

	// TODO: make pointer to current point in buffer to avoid unsigned long long int overflow
	// store payload
	if (masked) {
		for (unsigned long long int byte = currByteOffset; byte < currByteOffset + payloadLen; byte++) { // '<' because (currByteOffset + length) is 1 too many
			socketPtr->buffer[byte] = (*payload)[byte - currByteOffset] ^ mask[(byte - currByteOffset) % 4];
		}
	} else {
		for (unsigned long long int byte = currByteOffset; byte < currByteOffset + payloadLen; byte++) { // '<' because (currByteOffset + length) is 1 too many
			socketPtr->buffer[byte] = (*payload)[byte - currByteOffset];
		}
	}

	// setup for select so write can timeout
	fd_set writeFDs;
	FD_ZERO(&writeFDs);
	FD_SET(socketPtr->socketFD, &writeFDs);

	// check if read is ready until timeout 
	int fdsReady = pselect(socketPtr->socketFD + 1, NULL, &writeFDs, NULL, &ioTimeout, NULL);
	if (fdsReady == ERROR) {
		return ERROR;
	} else if (fdsReady == 1) {
		if (write(socketPtr->socketFD, socketPtr->buffer, msgLen) == ERROR) {
			return ERROR;
		}
	} else { // timed out because only i FD in sets
		return ERROR;
	}

	return EXIT_SUCCESS;
}

/**
  * @breif - writes a close frame to the given socket.
  * @param - socketNode* socketPtr - the socket to send the close frame.
  * @param - unsigned short statusCode - the websockets defined status code.
  * @param - char* reason - the reason for the status code/close.
  * @return - int - 0 on success and -1 on error.
  */
int writeWebSocketCloseFrame(socketNode* socketPtr, unsigned short statusCode, char* reason) {
	char* payload;
	unsigned int length = sizeof(unsigned short) + strlen(reason);

	if (length > WS_DEFAULT_FRAME_MAX_LEN) {
		return ERROR;
	}

	payload = calloc(length, sizeof(char));

	for (int byte = 0; byte < length; byte++) {
		if (byte < sizeof(unsigned short)) {
			for (short bit = 7; bit >= 0; bit--) { // for each bit
				payload[byte] ^= (-((statusCode >> (sizeof(unsigned short) * BITS_IN_A_BYTE + bit)) & 0x01) ^ payload[byte]) & (1 << bit);
			}
		} else {
			payload[byte] = reason[byte];
		}
	}

	writeWebSocketFrame(socketPtr, &payload, length, false, true, WS_OP_CODE_CLOSE);
	free(payload);
}

/**
  * @breif - writes a ping frame to the given socket and stores relevant data in the socketNode if one hasn't already been sent.
  * @param - socketNode* socketPtr - the socket to send the ping frame.
  * @return - int - 0 on success and -1 on error.
  */
int writeWebSocketPingFrame(socketNode* socketPtr) {
	if (socketPtr->pingSent) {
		return EXIT_SUCCESS;
	}

	char* payload = calloc(DEF_PING_RAND_BYTES_COUNT, sizeof(char));
	if (RAND_bytes(payload, DEF_PING_RAND_BYTES_COUNT) != 1) {
		return ERROR;
	}
	memcpy(socketPtr->pingPayload, payload, DEF_PING_RAND_BYTES_COUNT);

	writeWebSocketFrame(socketPtr, &payload, DEF_PING_RAND_BYTES_COUNT, false, true, WS_OP_CODE_PING);

	clock_gettime(CLOCK_REALTIME, &(socketPtr->pingTime));
	socketPtr->pingSent = true;
	free(payload);

	return EXIT_SUCCESS;
}

// ~~~~~~~~~~~~~~~~~~~~ HTTP ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - sends an error message 400 Bad Request to the given socket. (socket must be closed afterwards and)
  * @param - socketNode* socketPtr - the socket to send the error message to.
  * @param - char* addedHeader - any estra headers to add to the response including CRLF.
  * @return - int - 0 on success and -1 on an error.
  */
int badRequestResponse(socketNode* socketPtr, char* addedHeader) {
	char* statusLine = "HTTP/1.1 400 Bad Request\r\n";
	char* headBodyDelim = "\r\n";

	// clear buffer
	bzero(socketPtr->buffer, sizeof(char) * bufferlen);

	// compile response
	strcat(socketPtr->buffer, statusLine);
	if (addedHeader != NULL) {
		strcat(socketPtr->buffer, addedHeader);
	}
	strcat(socketPtr->buffer, headBodyDelim);

	// setup for select so write can timeout
	fd_set writeFDs;
	FD_ZERO(&writeFDs);
	FD_SET(socketPtr->socketFD, &writeFDs);

	// check if read is ready until timeout 
	int fdsReady = pselect(socketPtr->socketFD + 1, NULL, &writeFDs, NULL, &ioTimeout, NULL);
	if (fdsReady == ERROR) {
		return ERROR;
	} else if (fdsReady == 1) {
		if (write(socketPtr->socketFD, socketPtr->buffer, strlen(socketPtr->buffer)) == ERROR) {
			return ERROR;
		}
	} else { // timed out because there is only 1 socket fd available
		return ERROR;
	}

	return EXIT_SUCCESS;
}

// ~~~~~~~~~~~~~~~~~~~~ wiringPi ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - initializes wiringPi and needed global variables.
  * @return - int - 0 on success and -1 on error.
  */
int initWiringPi() {
	if (wiringPiSetup() == ERROR) {
		return ERROR;
	}

	for (short pin = WIRING_PI_PIN_MIN; pin <= WIRING_PI_PIN_MAX; pin++) {
		pinUsedList[pin] = false;
	}

	pinWriteTimeout.tv_sec = DEF_PIN_WRITE_TIMEOUT_SEC;
	pinWriteTimeout.tv_nsec = DEF_PIN_WRITE_TIMEOUT_NSEC;
}

/**
  * @breif - parses the given buffer in the format "## ACTION VALUE" where ## is the wiringPi pin number and executes.
  * @param - socketNode* socketPtr - a pointer to the socket with the decoded payload in the buffer.
  * @return - int - 0 on success and -1 on error.
  */
int parseWiringPiBuffer(socketNode* socketPtr) {
	const char* BUFF_DELIM = " ";
	const int MAX_TOKS = 3;

	char* saveptr;
	char* currBuffTok;
	char* tempStrtolEndPtr;
	int motorIndex;
	int direction;
	int steps;
	long tempStrtolVal;

	currBuffTok = strtok_r(socketPtr->buffer, BUFF_DELIM, &saveptr); // break buffer by spaces and return wiringPi pin number
	for (int tokNum = 1; tokNum <= MAX_TOKS; tokNum++) {
		if (currBuffTok == NULL) {
			break; // all tokens proccessed
		}

		switch (tokNum) {
			case MOTOR_INDEX_TOK:
				motorIndex = strtol(currBuffTok, &tempStrtolEndPtr, 10);
				if (checkStrtolError(&motorIndex, currBuffTok, tempStrtolEndPtr) == ERROR
					|| motorIndex < 0
					|| motorIndex >= motorCount) {
					return ERROR; // invalid motor number
				}
				break;

			case DIRECTION_TOK:
				if ((strcmp(currBuffTok, "+") == 0)
					|| (((tempStrtolVal = strtol(currBuffTok, &tempStrtolEndPtr, 10)) == POS_DIRECTION) && checkStrtolError(&tempStrtolVal, currBuffTok, tempStrtolEndPtr) != ERROR)) {
					direction = POS_DIRECTION;
				} else if ((strcmp(currBuffTok, "-") == 0)
						   || (((tempStrtolVal = strtol(currBuffTok, &tempStrtolEndPtr, 10)) == NEG_DIRECTION) && checkStrtolError(&tempStrtolVal, currBuffTok, tempStrtolEndPtr) != ERROR)) {
					direction = NEG_DIRECTION;
				} else {
					return ERROR; // unspecified direction
				}
				break;

			case STEPS_TOK:
				steps = strtol(currBuffTok, &tempStrtolEndPtr, 10);
				if (checkStrtolError(&steps, currBuffTok, tempStrtolEndPtr) == ERROR
					|| steps < 1) {
					return ERROR; // invalid amount of steps
				}
				break;

			default:
				return ERROR; // no function for token
		}

		currBuffTok = strtok_r(NULL, BUFF_DELIM, &saveptr); // take next token
	}
	if (stepMotor(motorIndex, direction, steps) == ERROR) {
		return ERROR;
	} else {
		return EXIT_SUCCESS;
	}
}

/**
  * @berif - steps the identified motor in the given direction by the given steps or up to a limit.
  * @param - int motorIndex - the index of the motor in motors to step.
  * @param - int direction - the direction to step the given motor in.
  * @param - int steps - the amount of steps to attempt.
  * @return - the amount of steps not taken because a limit was reached or -1 on an error.
  */
int stepMotor(int motorIndex, int direction, int steps) {
	if (motorIndex < 0 || motorIndex >= motorCount) {
		return ERROR;
	} else if (direction != HIGH && direction != LOW) {
		return ERROR;
	} else if (steps < 1) {
		return ERROR;
	}
	
	// set to max steps if too many were asked for
	int allowedSteps = steps;
	if (stepLimit != 0 && allowedSteps > stepLimit) {
		allowedSteps = stepLimit;
	}

	int remainingSteps = 0;

	pthread_mutex_lock(&motors[motorIndex].lock);

	// change direction if necessary
	if (digitalRead(motors[motorIndex].directionPin) != direction) {
		digitalWrite(motors[motorIndex].directionPin, direction);
		if (nanosleep(&pinWriteTimeout, NULL) == ERROR) {
			pthread_mutex_unlock(&motors[motorIndex].lock);
			return ERROR;
		}
	}

	for (int step = 1; step <= allowedSteps; step++) {
		if (direction == POS_DIRECTION) {
			if (motors[motorIndex].posStepLimit < 0 || (motors[motorIndex].stepCount + 1) <= motors[motorIndex].posStepLimit) {
				motors[motorIndex].stepCount++;

				// send rising signal to step
				if (digitalRead(motors[motorIndex].stepPin) != LOW) {
					digitalWrite(motors[motorIndex].stepPin, LOW);
					if (nanosleep(&pinWriteTimeout, NULL) == ERROR) {
						pthread_mutex_unlock(&motors[motorIndex].lock);
						return ERROR;
					}
				}
				digitalWrite(motors[motorIndex].stepPin, HIGH);
				if (nanosleep(&pinWriteTimeout, NULL) == ERROR) {
					pthread_mutex_unlock(&motors[motorIndex].lock);
					return ERROR;
				}
			} else {
				remainingSteps = steps - (step - 1);
				break;
			}
		} else if (direction == NEG_DIRECTION) {
			if (motors[motorIndex].negStepLimit > 0 || (motors[motorIndex].stepCount - 1) >= motors[motorIndex].negStepLimit) {
				motors[motorIndex].stepCount--;

				// send rising signal to step
				if (digitalRead(motors[motorIndex].stepPin) != LOW) {
					digitalWrite(motors[motorIndex].stepPin, LOW);
					if (nanosleep(&pinWriteTimeout, NULL) == ERROR) {
						pthread_mutex_unlock(&motors[motorIndex].lock);
						return ERROR;
					}
				}
				digitalWrite(motors[motorIndex].stepPin, HIGH);
				if (nanosleep(&pinWriteTimeout, NULL) == ERROR) {
					pthread_mutex_unlock(&motors[motorIndex].lock);
					return ERROR;
				}
			} else {
				remainingSteps = steps - (step - 1);
				break;
			}
		} else {
			pthread_mutex_unlock(&motors[motorIndex].lock);
			return ERROR;
		}
	}
	pthread_mutex_unlock(&motors[motorIndex].lock);
	return remainingSteps;
}

/**
  * @breif - destroys or deallocates vars created for wiringPi (called after threads have ended).
  * @return - int - 0 on success and -1 on error.
  */
int destroyWiringPi() {
	for (short motor = 0; motor < motorCount; motor++) {
		if (pthread_mutex_destroy(&motors[motor].lock) != 0) {
			error("ERROR on destroying motor mutexes\n");
		}
	}
	free(motors);
	return EXIT_SUCCESS;
}

// ~~~~~~~~~~~~~~~~~~~~ Outsourced ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - encodes to base 64.
  * @param - const void* data_buf - the data to be encoded.
  * @param - size_t dataLength - the length of the data to be encoded.
  * @param - char* result - the encoded data.
  * @param - size_t resultSize - the length of the result buffer.
  * @return - int - 0 on success and -1 when the buffer is too small.
  * @sourced - https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64#C - 21/01/2016
  */
int base64encode(const void* DATA_BUF, size_t dataLength, char* result, size_t resultSize) {
	const char base64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	const uint8_t *data = (const uint8_t *)DATA_BUF;
	size_t resultIndex = 0;
	size_t x;
	uint32_t n = 0;
	int padCount = dataLength % 3;
	uint8_t n0, n1, n2, n3;

	/* increment over the length of the string, three characters at a time */
	for (x = 0; x < dataLength; x += 3) {
		/* these three 8-bit (ASCII) characters become one 24-bit number */
		n = (uint32_t)data[x] << 16;

		if ((x + 1) < dataLength)
			n += (uint32_t)data[x + 1] << 8;

		if ((x + 2) < dataLength)
			n += data[x + 2];

		/* this 24-bit number gets separated into four 6-bit numbers */
		n0 = (uint8_t)(n >> 18) & 63;
		n1 = (uint8_t)(n >> 12) & 63;
		n2 = (uint8_t)(n >> 6) & 63;
		n3 = (uint8_t)n & 63;

		/*
		* if we have one byte available, then its encoding is spread
		* out over two characters
		*/
		if (resultIndex >= resultSize) return ERROR;   /* buffer too small */
		result[resultIndex++] = base64chars[n0];
		if (resultIndex >= resultSize) return ERROR;   /* buffer too small */
		result[resultIndex++] = base64chars[n1];

		/*
		* if we have only two bytes available, then their encoding is
		* spread out over three chars
		*/
		if ((x + 1) < dataLength) {
			if (resultIndex >= resultSize) return ERROR;   /* buffer too small */
			result[resultIndex++] = base64chars[n2];
		}

		/*
		* if we have all three bytes available, then their encoding is spread
		* out over four characters
		*/
		if ((x + 2) < dataLength) {
			if (resultIndex >= resultSize) return ERROR;   /* buffer too small */
			result[resultIndex++] = base64chars[n3];
		}
	}

	/*
	* create and add padding that is required if we did not have a multiple of 3
	* number of characters available
	*/
	if (padCount > 0) {
		for (; padCount < 3; padCount++) {
			if (resultIndex >= resultSize) return ERROR;   /* buffer too small */
			result[resultIndex++] = '=';
		}
	}
	if (resultIndex >= resultSize) return ERROR;   /* buffer too small */
	result[resultIndex] = 0;
	return 0;   /* indicate success */
}

/**
  * @breif - returns the time difference from start to end timespec.
  * @param - struct timespec start - the timespec set at the beginning of the timed code.
  * @param - struct timespec end - the timespec set at the end of the timed code.
  * @return - struct timespec - the difference of time from start to end.
  * @sourced - https://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/ - 4/02/2016
  */
struct timespec timespecDiff(struct timespec start, struct timespec end) {
	struct timespec temp;
	if ((end.tv_nsec - start.tv_nsec) < 0) {
		temp.tv_sec = end.tv_sec - start.tv_sec - 1;
		temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec - start.tv_sec;
		temp.tv_nsec = end.tv_nsec - start.tv_nsec;
	}
	return temp;
}