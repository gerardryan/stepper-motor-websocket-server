# README #

### To Install ###

**Install required libraries**
```
sudo apt-get install wiringpi
```
```
sudo apt-get install libssl-dev
```


**Update system**
```
sudo apt-get update
```
```
sudo apt-get upgrade
```


**Run the build file**
```
./build
```


**Run the server**
```
sudo ./stepperMotorServer -m 0 1 2 1 3 0 0 60 -60
```


**For CLI help**
```
sudo ./stepperMotorServer -h
```


### What is this repository for? ###
This a simple server designed to allow remote communication to 1 or more [EasyDriver stepper motor driver](http://www.schmalzhaus.com/EasyDriver/) connected to a raspberry pi via the websockets protocol aka from the web browser.

### Websockets command specification ###
* This implementation follows the wiringPi GPIO pin numbering as shown [here](https://projects.drogon.net/raspberry-pi/wiringpi/pins/).
* For setting up a websocket connection see */www_example/index.html* or go [here](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications).
* The current implementation takes messages/commands in the form `MOTOR_INDEX DIRECTION STEPS` e.g. `0 + 10` where `MOTOR_INDEX` is the motor added by argument `-m`, `DIRECTION` is the direction for the given motor to spin and `STEPS` is the requested amount of steps for the given motor to step in the given direction.
* The `MOTOR_INDEX` corresponds with the ordering of `-m` arguments given to the program. (e.g. first `-m` will add the motor to the `0` index and so on)
* Amount of motors is not limited by software but the pin numbers currently available range from `0` to `20`.
* The Directions available are `+` or `1` for the positive direction and `-` or `0` for the negative direction.

### CLI help ###
Usage: `sudo ./stepperMotorServer[OPTIONS]`

Example: `sudo ./stepperMotorServer -m 0 1 2 1 3 0 0 60 -60`

| Option                        | Description                                                                                                                                                                                                    | Alt Option        |
| ----------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
| `--help`                      | Displays help message.                                                                                                                                                                                         | `-h`              |
| `--port NUM`                  | Port for the server to listen on.                                                                                                                                                                              | `-p NUM`          |
| `--backlog NUM`               | The length of the backlog of unhandled connection.                                                                                                                                                             | `-b NUM`          |
| `--threads NUM`               | Number of threads to used to process connections.                                                                                                                                                              | `-t NUM`          |
| `--buffer-size NUM`           | Buffer size for every socket.                                                                                                                                                                                  | `-bs NUM`         |
| `--ip-limit NUM`              | The limit of same ip address connections.                                                                                                                                                                      | `-il NUM`         |
| `--ping-timeout NUM x 2`      | How long to wait in seconds and nanoseconds for a response/pong before disconnecting. NOTE: This should allow for the time of any non control frame operations that could occur especially any using timeouts. | `-pt NUM x 2`     |
| `--io-timeout NUM x 2`        | How long to wait in seconds and nanoseconds for a message before giving up and pinging.                                                                                                                        | `-it NUM x 2`     |
| `--pin-write-timeout NUM x 2` | The amount of time seconds and nanoseconds to lock a pin for after being written to.                                                                                                                           | `-pwt NUM x 2`    |
| `--step-limit NUM`            | The number of steps allowed by one message received. (set 0 for unlimited)                                                                                                                                     | `-sl NUM`         |
| `-motor NUM x 7 to 9`         | Add a motor to control. (See table below for argument explanation)                                                                                                                                             | `-m NUM x 7 to 9` | 

#### `-motor` arguments ####

| Argument | Description                                                                                                                                                       |
| -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `NUM 1`  | Direction pin i.e. The wiringPi defined output pin where 1/HIGH will cause the motor to step in the positive direction and 0/LOW in the negative.                 |
| `NUM 2`  | Step pin i.e. The wiringPi defined output pin where a rising signal will cause a step in this motor.                                                              |
| `NUM 3`  | Micro step pin one i.e. The wiringPi defined output pin followed by a bit value e.g. "7 1". (set pin negative and no value if not needed)                         |
| `NUM 4`  | OPTIONAL: if the pervious argument is set non-negative this must have a value of 0 or 1 otherwise skip. (`-m 0 1 -1 2 0 0 60 -60` or `-m 0 1 2 1 3 0 0 60 -60`)   |
| `NUM 5`  | Micro step pin two i.e. The wiringPi defined output pin followed by a bit value e.g. "4 0". (set pin negative and no value if not needed)                         |
| `NUM 6`  | OPTIONAL: if the pervious argument is set non-negative this must have a value of 0 or 1 otherwise skip. (`-m 0 1 2 1 -1 0 0 60 -60` or `-m 0 1 2 1 3 0 0 60 -60`) |
| `NUM 7`  | Step count i.e. Where the motor is compared to the motors zero position. (set 0 if motor is in default position)                                                  |
| `NUM 8`  | Positive step limit i.e. The most Step count can go in the positive direction. (set negative if not needed)                                                       |
| `NUM 9`  | Negative step limit i.e. The most Step count can go in the negative direction. (set positive if not needed)                                                       |

NOTE: ERROR will be thrown on the same pin used in twice.