#ifndef STEPPERMOTORSERVER_H
#define STEPPERMOTORSERVER_H
#define _GNU_SOURCE

// Libraries
#include <arpa/inet.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include <openssl/rand.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <wiringPi.h>

// Constants
#define DEF_PORT 12345
#define DEF_BACKLOG 20
#define DEF_NUM_THREADS 10
#define DEF_BUFFER_LENGTH 2048
#define ERROR -1
#define SAME_IP_LIMIT 5
#define BITS_IN_A_BYTE 8
#define MOST_SIGNIFICANT_BIT 7
#define DEF_IO_TIMEOUT_SEC 0
#define DEF_IO_TIMEOUT_NSEC 700000000 // 700ms
#define DEF_PING_RAND_BYTES_COUNT 4
#define DEF_PING_TIMEOUT_SEC 1
#define DEF_PING_TIMEOUT_NSEC 500000000 // 500ms

// Websocket constants
#define WS_VERSION 13.0
#define WS_CONTROLL_FRAME_RETURN 0
#define WS_DEFAULT_FRAME_MAX_LEN 125
#define WS_MIN_FRAME_SIZE_BYTES 2

// Information bit/byte constants
#define WS_INFO_BYTE 0
#define WS_FIN_BIT 7
#define WS_RSV1_BIT 6
#define WS_RSV2_BIT 5
#define WS_RSV3_BIT 4
#define WS_OP_CODE_START 3
#define WS_OP_CODE_END 0

// Mask and default length constants
#define WS_MASK_DEF_LEN_BYTE 1
#define WS_MASK_BIT 7
#define WS_DEF_LEN_BIT_START 6
#define WS_DEF_LEN_BIT_END 0
#define WS_MASK_SIZE_BYTES 4

// Extended length constants
#define WS_LEN_EXT1 126
#define WS_LEN_EXT1_MAX 0xFFFF // two bytes all bits set
#define WS_LEN_EXT2 127
#define WS_LEN_EXT_BYTE_START 2
#define WS_LEN_EXT1_BYTE_END 3
#define WS_LEN_EXT1_BYTE_LEN 2
#define WS_LEN_EXT2_BYTE_END 9
#define WS_LEN_EXT2_BYTE_LEN 8

// WebSockets opperation code constants
#define WS_OP_CODE_CONTROLL_FRAME 0x8
#define WS_OP_CODE_CONTINUATION 0x0
#define WS_OP_CODE_TEXT 0x1
#define WS_OP_CODE_BINARY 0x2
#define WS_OP_CODE_CLOSE 0x8
#define WS_OP_CODE_PING 0x9
#define WS_OP_CODE_PONG 0xA
#define WS_OP_CODE_INVALID 0x10

//Structures 
typedef struct threadPool {
	struct threadNode* head;
	bool killThreads;
} threadPool;

typedef struct threadNode {
	pthread_t threadId;
	struct threadNode* next;
} threadNode;

typedef struct socketList {
	struct socketNode* head;
	struct socketNode* tail;
	pthread_mutex_t listMutex;
	sem_t count;
} socketList;

typedef struct socketNode {
	int socketFD;
	bool doHandshake;
	bool pingSent;
	struct timespec pingTime;
	char pingPayload[DEF_PING_RAND_BYTES_COUNT];
	char* socketIP;
	struct socketNode* next;
	char* buffer;
} socketNode;

typedef struct socketRefList {
	struct socketRefNode* head;
	pthread_mutex_t listMutex;
} socketRefList;

typedef struct socketRefNode {
	char* socketIP;
	struct socketRefNode* next;
} socketRefNode;

// Global Variables 
int listenFD;
long numThreads;
long backlog;
unsigned long long int bufferlen;
long port;
long ipLimit;
threadPool pool;
socketList sockets;
socketRefList connected;
struct timespec ioTimeout;
struct timespec pingTimeout;

// ~~~~~~~~~~~~~~~~~~~~ wiringPi ~~~~~~~~~~~~~~~~~~~~
#define WIRING_PI_PIN_MIN 0
#define WIRING_PI_PIN_MAX 20
#define DEF_PIN_WRITE_TIMEOUT_SEC 0
#define DEF_PIN_WRITE_TIMEOUT_NSEC 100000000 // 100ms
#define RISING_SIGNAL_EDGE 1
#define FALLING_SIGNAL_EDGE 0
#define NO_SIGNAL_EDGE 2

bool pinUsedList[WIRING_PI_PIN_MAX + 1];
struct timespec pinWriteTimeout;

// ~~~~~~~~~~~~~~~~~~~~ Motor ~~~~~~~~~~~~~~~~~~~~
#define POS_DIRECTION 1
#define NEG_DIRECTION 0
#define DEF_STEP_LIMIT 10
#define MOTOR_INDEX_TOK 1
#define DIRECTION_TOK 2
#define STEPS_TOK 3

typedef struct motor {
	unsigned int directionPin;
	unsigned int stepPin;
	long stepCount;
	long posStepLimit;
	long negStepLimit;
	pthread_mutex_t lock;
} motor;

motor* motors;
unsigned int motorCount;
unsigned int stepLimit;

// Method headers

/**
  * @breif - Parses the given CLI arguments (argv)
  * @param - int argc - the number of CLI arguments
  * @param - char* argv[] - the CLI arguments
  */
void parseArgs(int argc, char* argv[]);

/**
  * @breif - prints the help message and exits
  */
void help();

/**
  * @breif - Makes the socket for the server to listen on and returns it.
  * @param - int portNum - The desired port number for the machine.
  * @return - int - Returns the listening server socket.
  */
int makeServerSocket(int portNum);

/**
  * @breif - Prints out an error message.
  * @param - const char* msg - The message to be printed.
  */
void error(const char* msg);

/**
  * @breif - given the return value of strtol() or strtoll() will check if an error occured.
  * @param - void* strtolVal - the casted address of the return value from strtol() or strtoll().
  * @param - char* nptr - the c-string strtol() or strtoll() parsed.
  * @param - char* endptr - the c-string set after strtol() or strtoll().
  * @return - int - 0 on success and -1 on error within the strtol() or strtoll() call.
  */
int checkStrtolError(void* strtolVal, char* nptr, char* endptr);

/**
  * @breif - Overrides the interrupt signal to close open sockets and free memory allocated in the heap before exiting.
  */
void interruptHandler();

// ~~~~~~~~~~~~~~~~~~~~ socket List ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - Returns an initialized and empty, list of sockets.
  * @return - socketList - The initialized list for sockets.
  */
socketList initSocketList();

/**
  * @breif - Returns an initialized and empty, list of sockets refrences.
  * @return - socketRefList - The initialized list of sockets refrences.
  */
socketRefList initSocketRefList();

/**
  * @breif - Adds a socket to the end of a given socket list.
  * @param - socketList* listPtr - A pointer to the list of sockets to add a new socket to.
  * @param - int socketFD - The socket file descriptor to be added.
  * @param - char* socketIP - socket ip address used to remove from connected list.
  */
void addSocket(socketList* listPtr, int socketFD, char* socketIP, socketRefList* refListPtr);

/**
  * @breif - Adds a socket refrence to the end of the given socket refrence list.
  * @param - socketRefList* listPtr - A pointer to the list of sockets refrences to add a new socket refrence to.
  * @param - char* socketIP - the ip of the connecting socket, used to prevent a DoS attack.
  */
void addSocketRef(socketRefList* listPtr, char* socketIP);

/**
  * @breif - Returns and removes a socket node from the list of sockets (Method waits on a semaphore).
  * @param - socketList* listPtr - A pointer to the list to retrieve a socket from.
  * @param - threadPool* poolPtr - A pointer to the pool in whitch this method is running.
  * @return - socketNode* - The socketNode that has been taken from the list or null if thread is exiting.
  */
socketNode* takeSocket(socketList* listPtr, threadPool* poolPtr);

/**
  * @breif - Adds a socket to the end of a given socket list.
  * @param - socketNode* listPtr - A pointer to the list of sockets to return the socket to.
  * @param - socketNode* socket - The socket to be returned.
  */
void returnSocket(socketList* listPtr, socketNode* socket);

/**
  * @breif - Closes the given socket, frees the malloc for the node and removes the socket refrence (Node must be removed from the socket list).
  * @param - socketNode* socket - The socket to be closed and freed.
  * @param - socketRefList* refList - the list the socket refrences are stored in.
  */
void closeSocket(socketNode* socketPtr, socketRefList* refList);

/**
  * @breif - removes the refrence for the given socket.
  * @param - socketNode* socket - The socket to being closed and needs the refrence removed.
  * @param - socketRefList* refList - the list of socket refrences to remove from.
  */
void removeSocketRefrence(socketNode* socketPtr, socketRefList* refList);

/**
  * @breif - counts the amount of times the current ip has connects and returns true if greater than connectLimit false otherwise.
  * @param - char* socketIP - the ip address to be tested.
  * @param - socketRefList* refListPtr - the list of socket refrences, aka all sockets connected.
  * @param - int connectLimit - the amount of times the same ip can connect but not exceede.
  * @return - bool - weather the given socket had connected too many times or not.
  */
bool connectedTooManyTimes(char* socketIP, socketRefList* refListPtr, int connectLimit);

/**
  * @breif - Deallocates all memory held by the given socket list (list must be complete e.g no socket used by a thread ideally threads not running).
  * @param - socketList* listPtr - A pointer to the list of sockets to be deleted or deallocated.
  */
void deallocateSocketList(socketList* listPtr);

/**
  * @breif - Deallocates the socket refrence list.
  * @param - socketList* listPtr - A pointer to the socket refrence list to be deleted or deallocated.
  */
void deallocateRefSocketList(socketRefList* listPtr);

// ~~~~~~~~~~~~~~~~~~~~ Thread pool ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - Returns an initialized thread pool (initialized without threads).
  * @return - threadPool - The initialized thread pool.
  */
threadPool initThreadPool();

/**
  * @breif - Adds a thread to the given thread pool
  * @param - threadPool* poolPtr - A pointer to the pool to add a new thread to.
  */
void addThread(threadPool* poolPtr);

/**
  * @breif - Deallocates all memory held by the given pool and ends the threads.
  * @param - threadPool* poolPtr - The pointer to the thread pool to be deleted or deallocated.
  * @param - sem_t* semPtr - The pointer to the semaphore the threads will be waiting on.
  */
void deallocateThreadPool(threadPool* poolPtr, sem_t* semPtr);

// ~~~~~~~~~~~~~~~~~~~~ WebSockets ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - preforms the Websockets Handshake as specified by standards rfc6455
  * @return - int - returns 0 on succesful Handshake and -1 on error or rejected
  */
int handshake();

/**
  * @breif - cyphers key and sends a standard accepted websocket upgrade response.
  * @param - socketNode* socketPtr - the socket to send the response to.
  * @param - char* wsKey - the websocket key used to unlock websocket functionality.
  * @return - int - 0 on success, -1 on an error.
  */
int acceptWebSocketResponse(socketNode* socketPtr, char* wsKey);

/**
  * @breif - Reads any frames recieved from the given socket and fills its buffer with any recieved data.
  * @param - socketNode* socketPtr - the socket to read and save any recieved data to.
  * @param - bool* isBinary - a flag indicating weather the returned data is binary or text.
  * @return - long long int - 0 on controll frames handled or no payload, -1 on error or socket closing and greater than 0 identifying the data length.
  */
long long int readWebSocketData(socketNode* socketPtr, bool* isBinary);

/**
  * @breif - reads/decodes a websocket frame and stores extracted data.
  * @param - socketNode* socketPtr - the socket to read the frame from.
  * @param - char** payload - where the decoded payload will be stored.
  * @param - bool* finished - where weather it is a finish frame or not will be stored.
  * @param - unsigned short int* opCode - where the opperation code will be stored.
  * @return - long long int - 0 on unresponsive and ping sent, -1 on an error and the length of the payload otherwise.
  */
long long int readWebSocketFrame(socketNode* socketPtr, char** payload, bool* finished, unsigned short int* opCode);

/**
  * @breif - encodes/writes a websocket frame with the given data.
  * @param - socketNode* socketPtr - the socket to write the frame to.
  * @param - char** payload - the payload to be sent.
  * @param - unsigned long long int payloadLen - the length of the given payload.
  * @param - bool masked - weather to mask the frame payload or not.
  * @param - bool finished - weather it is a finish/controll frame or not.
  * @param - unsigned short int opCode - the opperation code for the frame.
  * @return - int - 0 on success or -1 on an error.
  */
int writeWebSocketFrame(socketNode* socketPtr, char** payload, unsigned long long int payloadLen, bool masked, bool finished, unsigned short int opCode);

/**
  * @breif - writes a close frame to the given socket.
  * @param - socketNode* socketPtr - the socket to send the close frame.
  * @param - unsigned short statusCode - the websockets defined status code.
  * @param - char* reason - the reason for the status code/close.
  * @return - int - 0 on success and -1 on error.
  */
int writeWebSocketCloseFrame(socketNode* socketPtr, unsigned short statusCode, char* reason);

/**
  * @breif - writes a ping frame to the given socket and stores relevant data in the socketNode if one hasn't already been sent.
  * @param - socketNode* socketPtr - the socket to send the ping frame.
  * @return - int - 0 on success and -1 on error.
  */
int writeWebSocketPingFrame(socketNode* socketPtr);

// ~~~~~~~~~~~~~~~~~~~~ HTTP ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - sends an error message 400 Bad Request to the given socket. (socket should be closed afterwards)
  * @param - socketNode* socketPtr - the socket to send the error message to.
  * @param - char* addedHeader - any estra headers to add to the response including CRLF.
  * @return - int - 0 on success and -1 on an error.
  */
int badRequestResponse(socketNode* socketPtr, char* addedHeader);

// ~~~~~~~~~~~~~~~~~~~~ wiringPi ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif - initializes wiringPi and needed variables.
  * @return - int - 0 on success and -1 on error.
  */
int initWiringPi();

/**
  * @breif - parses the given buffer in the format "## ACTION VALUE" where ## is the wiringPi pin number and executes.
  * @param - socketNode* socketPtr - a pointer to the socket with the decoded payload in the buffer.
  * @return - int - 0 on success and -1 on error.
  */
int parseWiringPiBuffer(socketNode* socketPtr);

/**
  * @berif - steps the identified motor in the given direction by the given steps or up to a limit.
  * @param - int motorIndex - the index of the motor in motors to step.
  * @param - int direction - the direction to step the given motor in.
  * @param - int steps - the amount of steps to attempt.
  * @return - the amount of steps not taken because a limit was reached or -1 on an error.
  */
int stepMotor(int motorIndex, int direction, int steps);

/**
  * @breif - destroys or deallocates vars created for wiringPi (called after threads have ended).
  * @return - int - 0 on success and -1 on error.
  */
int destroyWiringPi();

// ~~~~~~~~~~~~~~~~~~~~ Outsourced ~~~~~~~~~~~~~~~~~~~~
/**
  * @breif -
  * @param - const void* data_buf -
  * @param - size_t dataLength -
  * @param - char* result -
  * @param - size_t resultSize -
  * @return - int -
  * @sourced - https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64#C - 21/01/2016
  */
int base64encode(const void* data_buf, size_t dataLength, char* result, size_t resultSize);

/**
  * @breif - returns the time difference from start to end timespec.
  * @param - struct timespec start - the timespec set at the beginning of the timed code.
  * @param - struct timespec end - the timespec set at the end of the timed code.
  * @return - struct timespec - the difference of time from start to end.
  * @sourced - https://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/ - 4/02/2016
  */
struct timespec timespecDiff(struct timespec start, struct timespec end);

#endif
